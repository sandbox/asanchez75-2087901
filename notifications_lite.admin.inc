<?php

/**
 * @file Contains functions for admin interface
 * Implementation of hook_menu
 * @return [type] [description]
 */
function notifications_lite_menu() {
  $items['user/%user/notifications/digest'] = array(
    'title' =>  'Digest settings',
    'description' => 'User digest settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('notifications_lite_digest_form', 1),
    'access arguments' => array('access content'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 100,
  );
  return $items;
}


/**
 * Implementation of form
 * @return [type] [description]
 */
function notifications_lite_digest_form($form, $form_state, $user) {

  $digest = notifications_lite_load($user->uid);

  $form['notifications_lite_uid'] = array(
    '#type' => 'hidden',
    '#value' => $user->uid,
  );

  $form['notifications_lite_status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable'),
    '#description' => t('Enable mode digest and disable another subscriptions types or vice versa'),
    '#default_value' => isset($digest['status']) ? $digest['status'] : 0,
  );

  $form['notifications_lite_digest_interval'] = array(
    '#title' => t('Digest interval'),
    '#type' => 'select',
    '#options' => _notifications_lite_digest_interval(),
    '#default_value' => isset($digest['send_interval']) ? $digest['send_interval'] : '86400',
  );

  $form['notifications_lite_format'] = array(
    '#title' => t('Format'),
    '#type' => 'select',
    '#options' => array('full_html' => 'Full HTML', 'plain_text' => 'Plain text'),
    '#default_value' => isset($digest['format']) ? $digest['format'] : 'full_html',
  );

  $form['notifications_lite_language'] = array(
    '#title' => t('Language'),
    '#type' => 'select',
    '#options' => _notifications_lite_languages(),
    '#default_value' => isset($digest['language']) ? $digest['language'] : 'en',
  );

  $form['notifications_lite_created'] = array(
    '#type' => 'hidden',
    '#value' => isset($digest['created']) ? $digest['created'] : time(),
  );

  $form['notifications_lite_updated'] = array(
    '#type' => 'hidden',
    '#value' => time(),
  );

  $form['notifications_lite_submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}

/**
 * Implementation of hook_form_validate
 * @return [type] [description]
 */

function notifications_lite_digest_form_validate($form, $form_state) {
  $uid = $form_state['values']['notifications_lite_uid'];
  $num_deleted = db_delete('notifications_lite_digest')
    ->condition('uid', $uid)
    ->execute();
}


/**
 * Implementation of hook_form_submit
 * @return [type] [description]
 */
function notifications_lite_digest_form_submit($form, $form_state) {

  $record['uid'] = $form_state['values']['notifications_lite_uid'];
  $record['send_interval'] = $form_state['values']['notifications_lite_digest_interval'];
  $record['status'] = $form_state['values']['notifications_lite_status'];
  $record['language'] = $form_state['values']['notifications_lite_language'];
  $record['format'] = $form_state['values']['notifications_lite_format'];
  $record['created'] = $form_state['values']['notifications_lite_created'];
  $record['updated'] = $form_state['values']['notifications_lite_updated'];
  // when enable digest mode, disable another subscription types
  // for another subscription types, 2 means "inactive" according Notifications module, 1 means "active"
  $switch = array(0 => 1, 1 => 2);

  if (drupal_write_record('notifications_lite_digest', $record)) {
    $subscriptions_status_updated = db_update('notifications_subscription')
      ->fields(array(
        'status' => $switch[$record['status']],
      ))
      ->condition('uid', $record['uid'])
      ->execute();

    drupal_set_message(t('Settings updated'), 'status', FALSE);
  }

}

/**
 *
 * @return array language
 */
function _notifications_lite_languages() {
  $languages = language_list();
  foreach ($languages as $key => $value) {
    $items[$key] = $value->name;
  }
  return $items;
}

/**
 * Load a notifications digest by user
 * @param  integer $uid user uid
 * @return array
 */
function notifications_lite_load($uid) {
  $query = db_select('notifications_lite_digest', 'd');
  $query->fields('d');
  $query->condition('d.uid', $uid);
  $digest = $query->execute()->fetchAssoc();
  return $digest;
}

