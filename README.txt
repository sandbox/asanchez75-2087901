It provides a simple way to send scheduler notifications digest.
Uses all (sub)modules provided by notifications module (except notifications_scheduler), job_scheduler_trigger and views.

The module use 2 types of notifications:

a) Alerts
0 sec     ('Immediately'),
300 sec   ('Each 5 minutes'),
600 sec   ('Each 10 minutes'),
3600 sec  ('Every hour'),
43200 sec ('Twice a day'),

b) Digest

86400'   ('Daily')
604800   ('Weekly')
2592000  ('Monthly')

==================================================
Initial settings
==================================================

When you install this module, the following "instances" are created by default

a) 8 actions are installed (see admin/config/system/actions)

1. Send scheduled notifications alert (Inmediately) configure delete
2. Send scheduled notifications alert (5 min) configure delete
3. Send scheduled notifications alert (10 min)  configure delete
4. Send scheduled notifications alert (hourly)  configure delete
5. Send scheduled notifications alert (twice a day) configure delete
6. Send scheduled notifications digest (daily)  configure delete
7. Send scheduled notifications digest (weekly) configure delete
8. Send scheduled notifications digest (monthly)

b) 7 job scheduler triggers are created (see admin/config/system/job_scheduler)

1. 5 min alert (notifications_lite)        */5 * * * *
2. 10 min alert (notifications_lite)       */10 * * * *
3. Hourly alert (notifications_lite)       0 * * * *
4. Twice a day alert (notifications_lite)  0 */6 * * *
5. Daily digest (notifications_lite)       0 0 * * *
6. Weekly digest (notifications_lite)      0 0 * * 0
7. Monthly digest (notifications_lite)     0 0 1 * *

c) Next 9 trigger are asociated

7 trigger under admin/structure/trigger/job_scheduler  (to send notifications after some time interval)

1. (notifications_lite) Send scheduled notifications alert (5 min)
2. (notifications_lite) Send scheduled notifications alert (10 min)
3. (notifications_lite) Send scheduled notifications alert (hourly)
4. (notifications_lite) Send scheduled notifications alert (twice a day)
5. (notifications_lite) Send scheduled notifications digest (daily)
6. (notifications_lite) Send scheduled notifications digest (weekly)
7. (notifications_lite) Send scheduled notifications digest (monthly)

one trigger under admin/structure/trigger/node (to send notifications inmediately)

8. (notifications_lite) Send scheduled notifications alert (Inmediately)

and one more under admin/structure/trigger/comment (to send notifications inmediately)

9. (notifications_lite) Send scheduled notifications alert (Inmediately)

Also, this module use Views (admin/structure/views) for

a) Display a log of notifications sent (available under path 'notifications_lite_log')
b) Theming Node Digest (available under path 'nodes_digest')
c) Theming Comments Digest (available under path 'coments_digest')

and will configure some notifications settings by default (see admin/config/messaging/subscriptions)

==================================================
Installation
==================================================

a) Install this module
b) Go to admin/structure/block and enable "Page subscriptions" block inside some region of your layout where a full text view of a node will be available
c) Fill out some nodes and subscribe to some nodes using options available in Page subscriptions" block.
d) To manage your notifications settings go to your account under tab named "notifications".
e) You will see a new subTab named "Digest settings". If you enable this feature (digest), it will disable your previous notificacions. If you disable it, you will enable your previous subscriptions (alerts).
f) Be sure to configure your cron followting instructions available in http://drupal.org/cron

==================================================
How does it works?
==================================================

This module try to follow the same logic provided by notifications module using job scheduler trigger. But as I was not able to run the notification_scheduler module I had to code a simple module who be able to do the same thing: send scheduler notifications.

This module collects all notifications belong to the same user under an defined interval. With this purpose, the job scheduler trigger implements several cron triggers. When a job scheduler cron runs, make a callback to all notifications under the same interval of this cron. We have 2 exceptions, for inmediate interval we use "node publish" and "comment publish" instead of cron triggers.

When a notification is sent is registered in a log and put in queue using hook_cron_queue_info. Also, as this module uses views to build the mail body (for alerts or digest) is easy to fix it.

Regrettably, you will find some harcode inside mail body. To remove go to notifications_lite.mail.inc file and change it as you want.






